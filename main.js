$(document).ready(function(){
  console.log("Welcome to the Game");

  var player = $("#Player"); // Element player
  var scenario = $("#scenario"); // Element scenario
  var dx = 1; // Precisión parameters
  var dy = -1; // Precisión parameters

var canvas = document.getElementById("ball");
  var ctx = canvas.getContext("2d");
var ballRadius = 2;
var x = canvas.width/2;
var y = canvas.height-30;

  captureKeys();


/* INICIO función para controlar al jugador */


function drawBall() {
    ctx.beginPath();
    ctx.arc(x, y, ballRadius, 0, Math.PI*2);
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBall();

    if(x + dx > canvas.width-ballRadius || x + dx < ballRadius) {
        dx = -dx;
    }
    if(y + dy > canvas.height-ballRadius || y + dy < ballRadius) {
        dy = -dy;
    }

    x += dx;
    y += dy;
}

setInterval(draw, 10);








  function moveLeft() {
    player.css({left: player.position().left - dx});
    //player.animate({ left: player.position().left - dx}, "fast");
  }

  function moveRight() {
    player.css({left: player.position().left + dx});
    //player.animate({ left: player.position().left + dx}, "fast");

  }

  function moveDown() {
    player.css({top: player.position().top + dy});
    //player.animate({ top: player.position().top + dy}, "fast");
  }

  function moveUp() {
    player.css({top: player.position().top - dy});
    //player.animate({ top: player.position().top - dy}, "fast");
  }

  function jump() {
    // player.css({top: player.position().top - 8*dy});
     let startPoint = player.position();
     player.animate({ top: startPoint.top - 6*dy}, "fast");
     player.animate({ top: startPoint.top - 2*dy}, "fast");
     player.animate({ top: startPoint.top - dy}, "fast");
     player.animate({ top: startPoint.top}, "fast");

  }

/* FIN función para controlar al jugador */


/* INICIO función para capturar las teclas */
  function captureKeys() {
    $("body").keypress(function(){
      if (event.which == 97) {
        // A
      //  moveLeft();

      }

      if (event.which == 100) {
        // D
//        moveRight();
      }

      if (event.which == 115) {
        // S
  //      moveDown();
      }

      if (event.which == 119) {
        // W
    //    moveUp();
    //    player.hello();
      }

      if (event.which == 32) {
        // SPACE
        jump();
        //  bounce();
      }


      $( "#log" ).prepend( event.type + ": " +  event.which + "<br/>");

      return false;

    });
  }
/* FIN función para capturar las teclas */





});

// https://stackoverflow.com/questions/7298507/move-element-with-keypress-multiple
const Player = {
  el: document.getElementById('player'),
  x: 0,
  y: 0,
  speed: 30,
  move() {
    this.el.style.transform = `translate(${this.x}px, ${this.y}px)`;
  }
};

const K = {
  fn(ev) {
    const k = ev.which;
    if (k >= 37 && k <= 40) {
      ev.preventDefault();
      K[k] = ev.type === "keydown"; // If is arrow
    }
  }
};

const update = () => {
  let dist = K[38] && (K[37] || K[39]) || K[40] && (K[37] || K[39]) ? 0.707 : 1;
  dist *= Player.speed;
  if (K[37]) Player.x -= dist;
  if (K[38]) Player.y -= dist;
  if (K[39]) Player.x += dist;
  if (K[40]) Player.y += dist;
  Player.move();
}

document.addEventListener('keydown', K.fn);
document.addEventListener('keyup', K.fn);

(function engine() {
  update();
  window.requestAnimationFrame(engine);
}());
